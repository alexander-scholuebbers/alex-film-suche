FROM node as builder
RUN npm install @angular/cli -g
#RUN npm install esbuild

COPY . /myapp
WORKDIR /myapp
RUN npm install

RUN ng build
#/myapp/dist/browser

FROM node 
RUN npm install http-server -g
COPY --from=builder /myapp/dist/film-suche/browser /app
WORKDIR /app 

CMD ["http-server"]

EXPOSE 8080