import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { FilmModel } from './film-model';


@Injectable({
  providedIn: 'root',
})
export class FilmServiceService {

  constructor(private http: HttpClient) { }

  neueFilme = new EventEmitter<FilmModel>();

  getFilms(suche: string): void {
    this.http.get<FilmModel>(`https://www.omdbapi.com/?apikey=477bca08&s=${suche}`).subscribe(d => { this.neueFilme.emit(d) });
  }
}
