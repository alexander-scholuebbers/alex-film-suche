import { Component, OnInit } from '@angular/core';
import { FilmServiceService } from '../film-service.service';
import { Search } from '../film-model';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-film-tab',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './film-tab.component.html',
  styleUrl: './film-tab.component.css'
})
export class FilmTabComponent implements OnInit {

  suche: string = "";
  films?: Search[];

  constructor(private fs: FilmServiceService) { }

  ngOnInit(): void {
    this.fs.neueFilme.subscribe(d => this.films = d.Search);
  }

  startSuche() {
    this.fs.getFilms(this.suche);
  }
}
