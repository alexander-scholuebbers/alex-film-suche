import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmTabComponent } from './film-tab.component';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('FilmTabComponent', () => {
  let component: FilmTabComponent;
  let fixture: ComponentFixture<FilmTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FilmTabComponent],
      providers: [HttpClient, HttpHandler]
    })
      .compileComponents();

    fixture = TestBed.createComponent(FilmTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
