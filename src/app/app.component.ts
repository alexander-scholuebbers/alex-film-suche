import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { FilmTabComponent } from './film-tab/film-tab.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, FilmTabComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'film-suche';
}
