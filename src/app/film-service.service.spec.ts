import { TestBed } from '@angular/core/testing';

import { FilmServiceService } from './film-service.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('FilmServiceService', () => {
  let service: FilmServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({providers:[HttpClient,HttpHandler]});
    service = TestBed.inject(FilmServiceService
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
